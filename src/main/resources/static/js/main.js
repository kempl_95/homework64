'use strict';
const baseUrl = "http://localhost:5555";
const addForm = document.getElementById('add_form');
const taskOlTeg = document.getElementById('task_ol');
let taskArray = [];
function Task(data) {
    this.text = data.text;
}
function createTaskElement(task) {
    let taskLiElement = document.createElement("li");
    taskLiElement.className = "taskLi";
    taskLiElement.innerText = task.text;

    taskLiElement.addEventListener('dblclick', function () {
        taskLiElement.style.textDecoration = "line-through";
    });

    return taskLiElement;
}
function addTask(taskElement) {
    taskOlTeg.append(taskElement);
}
addForm.addEventListener('submit', onAddHandler);

function onAddHandler(e) {
    e.preventDefault();
    const form = e.target;
    const dataForm = new FormData(form);
    const data =  Object.fromEntries(dataForm);
    addTask(createTaskElement(new Task(data)));
    document.getElementById("add_text").value = '';
}
/*
function update() {
    for (let i=0; document.getElementsByClassName("taskLi");i++){

    }
}
*/